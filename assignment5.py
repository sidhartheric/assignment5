#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON/')
def bookJSON():
    r = json.dumps(books)
    return Response(r)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template("showBook.html", books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == "GET":
        return render_template("newBook.html")
    elif request.method == "POST":
        newName = request.form['name']
        newId = -1
        expectedId = 1
        for i in range(len(books)):
            curId = books[i]["id"]
            if curId != expectedId:
                newId = expectedId
                break
            expectedId += 1
        if newId == -1:
            newId = len(books) + 1
        newDict = {'title': newName, 'id': newId}
        books.append(newDict)
        return render_template("showBook.html", books = books)

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == "GET": 
        return render_template("editBook.html", id = book_id)
    elif request.method == "POST":
        newName = request.form['name']
        books[book_id - 1]["title"] = newName
        return render_template("showBook.html", books = books)
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == "GET": 
        return render_template("deleteBook.html", id = book_id)
    elif request.method == "POST":
        index = 0
        for i in range(len(books)):
            if books[i]["id"] == book_id:
                index == i
                break
        del books[index]
        return render_template("showBook.html", books = books)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

